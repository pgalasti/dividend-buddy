package com.galasti.dividendbuddy.http;

import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;

import com.galasti.dividendbuddy.R;
import com.galasti.dividendbuddy.dto.TickerDto;

import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.List;

import static org.junit.Assert.assertFalse;

@RunWith(AndroidJUnit4.class)
public class HttpFetchTickerTest {

    @Test
    public void fetchTickerTest() throws Exception {

        Context appContext = InstrumentationRegistry.getTargetContext();


        HttpFetchTicker fetchTicker = new HttpFetchTicker(appContext.getString(R.string.http_fetch_tickers));
        long start = System.currentTimeMillis();
        List<TickerDto> tickers = fetchTicker.fetchTickers();
        long stop = System.currentTimeMillis();

        long result = stop-start;
        System.out.println("=== Fetch Ticker Fetch Time: " + result + "ms ===");
        assertFalse(tickers.isEmpty());
    }
}
