package com.galasti.dividendbuddy.services;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

import com.galasti.dividendbuddy.R;

public class FetchTickerReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.d("FetchTickerReceiver", "onReceive");
        Toast.makeText(context, "Updating symbol database...", Toast.LENGTH_LONG).show();

        Intent tickerServiceIntent = new Intent(context, FetchTickerService.class);
        tickerServiceIntent.putExtra("url", context.getString(R.string.http_fetch_tickers));
        context.startService(tickerServiceIntent);
    }
}
