package com.galasti.dividendbuddy.services;

import android.app.IntentService;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.util.Log;

import com.galasti.dividendbuddy.dto.TickerDto;
import com.galasti.dividendbuddy.http.HttpFetchTicker;
import com.galasti.dividendbuddy.persistence.LastUpdate;
import com.galasti.dividendbuddy.persistence.Ticker;

import java.util.Date;
import java.util.List;

public class FetchTickerService extends IntentService {

    private static final String TAG = "FetchTickerService";

    protected HttpFetchTicker httpFetchTicker;

    public FetchTickerService() {
        super("FetchTickerService");

        Log.d(TAG, "Startup...");
        Log.d(TAG, "Startup complete.");
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        Log.d(TAG, "Service starting fetch...");

        String url = intent.getStringExtra("url");
        Log.d(TAG, String.format("GET: %s", url));

        this.httpFetchTicker = new HttpFetchTicker(url);

        List<TickerDto> tickers = this.httpFetchTicker.fetchTickers();

        Log.d(TAG, "Fetch Complete");

        saveTickers(tickers);
    }

    protected void saveTickers(List<TickerDto> tickers) {
        Log.d(TAG, "Saving tickers...");

        Ticker.deleteAll(Ticker.class);

        Ticker ticker;
        for(TickerDto tickerDto : tickers) {
            ticker = new Ticker(tickerDto.getSymbol(), tickerDto.getCompanyName());
            ticker.save();
        }

        LastUpdate.update(new LastUpdate(new Date()));

        Log.d(TAG, "TickerDto save complete.");
    }
}
