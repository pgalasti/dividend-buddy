package com.galasti.dividendbuddy.persistence;

import com.orm.SugarRecord;

import java.util.Date;

public class LastUpdate extends SugarRecord {

    private Date lastUpdateTimestamp;

    public LastUpdate() {

    }

    public LastUpdate(Date timestamp) {
        this.lastUpdateTimestamp = timestamp;
    }

    public Date getLastUpdateTimestamp() {
        return lastUpdateTimestamp;
    }

    public void setLastUpdateTimestamp(Date lastUpdateTimestamp) {
        this.lastUpdateTimestamp = lastUpdateTimestamp;
    }
}
