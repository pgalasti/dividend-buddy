package com.galasti.dividendbuddy.persistence;

import com.orm.SugarRecord;

public class Ticker extends SugarRecord {

    protected String symbol;
    protected String companyName;

    public Ticker() {
    }

    public Ticker(String symbol, String companyName) {
        super();
        this.symbol = symbol;
        this.companyName = companyName;
    }

    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }
}
