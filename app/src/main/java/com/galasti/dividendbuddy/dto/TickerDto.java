package com.galasti.dividendbuddy.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.Date;

@JsonIgnoreProperties(ignoreUnknown = true)
public class TickerDto implements Serializable {

    @JsonProperty("symbol")
    private String symbol;

    @JsonProperty("name")
    private String companyName;

    @JsonProperty("date")
    private Date fetchDate;

    public TickerDto() {
        this.symbol = "";
        this.companyName = "";
        this.fetchDate = new Date();
    }

    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public Date getFetchDate() {
        return fetchDate;
    }

    public void setFetchDate(Date fetchDate) {
        this.fetchDate = fetchDate;
    }
}
