package com.galasti.dividendbuddy.dto;

import android.util.Log;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

@JsonIgnoreProperties(ignoreUnknown = true)
public class DividendStatsDto implements Serializable {

    @JsonProperty("dividendRate")
    private BigDecimal dividendRate;

    @JsonProperty("dividendYield")
    private BigDecimal dividendYield;

    @JsonProperty("exDividendDate")
    private String latestExDividendDateStr;

    private DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");

    public DividendStatsDto() {

    }

    public BigDecimal getDividendRate() {
        return dividendRate;
    }

    public void setDividendRate(BigDecimal dividendRate) {
        this.dividendRate = dividendRate;
    }

    public BigDecimal getDividendYield() {
        return dividendYield;
    }

    public void setDividendYield(BigDecimal dividendYield) {
        this.dividendYield = dividendYield;
    }

    public String getlatestExDividendDateStr() {
        return latestExDividendDateStr;
    }

    public void setlatestExDividendDateStr(String latestExDividendDateStr) {
        this.latestExDividendDateStr = latestExDividendDateStr;
    }

    public Date getExDateObj() {
        Date returnValue;
        try {
            returnValue = dateFormat.parse(this.latestExDividendDateStr);
        } catch(ParseException e) {
            Log.e("DividendStatsDto", "Unable to parse ex-dividend date into object!");
            returnValue = new Date();
        }
        return returnValue;
    }
}