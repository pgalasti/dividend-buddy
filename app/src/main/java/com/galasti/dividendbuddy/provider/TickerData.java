package com.galasti.dividendbuddy.provider;

import android.content.ContentResolver;
import android.net.Uri;

public class TickerData {

    public static final Uri
            CONTENT_URI = Uri.withAppendedPath(TickerContract.CONTENT_URI, "ticker");

    public static final String CONTENT_TYPE =
            ContentResolver.CURSOR_DIR_BASE_TYPE + "/vnd.com.galasti.dividendmanager.providers.tickerdata";

    public static final String CONTENT_TICKER_TYPE =
            ContentResolver.CURSOR_ITEM_BASE_TYPE +
                    "/vnd.com.galasti.dividendmanager.providers.tickerdata";

    public static final String[] PROJECTION_ALL = {"id", "company_name", "symbol"};

    public static final String SORT_ORDER_DEFAULT =
            "symbol" + " ASC";

    public TickerData() {

    }

    public static final String TABLE_NAME = "ticker";
    public static final String _ID = "id";
    public static final String _COMPANY_NAME = "company_name";
    public static final String _SYMBOL= "symbol";
}
