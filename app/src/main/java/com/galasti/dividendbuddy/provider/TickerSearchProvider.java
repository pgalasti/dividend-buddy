package com.galasti.dividendbuddy.provider;

import android.app.SearchManager;
import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;

import com.galasti.dividendbuddy.persistence.Ticker;
import com.galasti.dividendbuddy.util.TickerBestGuessScore;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class TickerSearchProvider extends ContentProvider {

    final static String PROVIDER = "com.galasti.dividendbuddy.provider";
    final static String URL = "content://" + PROVIDER + "/ticker";
    final static Uri CONTENT_URI = Uri.parse(URL);

    private static final int TICKER_DATA = 1;
    private static final int TICKER_DATA_ID = 2;
    static final UriMatcher URI_MATCHER;
    static {
        URI_MATCHER = new UriMatcher(UriMatcher.NO_MATCH);
        URI_MATCHER.addURI(PROVIDER, "TickerSearchProvider/" + SearchManager.SUGGEST_URI_PATH_QUERY, TICKER_DATA);
        URI_MATCHER.addURI(PROVIDER, "tickerdata/#", TICKER_DATA_ID);
    }

    private TickerBestGuessScore scorer = new TickerBestGuessScore();

    @Override
    public boolean onCreate() {
        return true;
    }

    @Nullable
    @Override
    public Cursor query(@NonNull Uri uri, @Nullable String[] projection, @Nullable String selection, @Nullable String[] selectionArgs, @Nullable String sortOrder) {

        String searchSegment = uri.getLastPathSegment();
        if(SearchManager.SUGGEST_URI_PATH_QUERY.equalsIgnoreCase(searchSegment)) {
            return null;
        }

        if(TextUtils.isEmpty(sortOrder)) {
            sortOrder = TickerData.SORT_ORDER_DEFAULT;
        }

        StringBuilder sbSymbolWhereClause = new StringBuilder("symbol LIKE '")
                .append(searchSegment)
                .append("%'");
        StringBuilder sbCompanyWhereClause = new StringBuilder("");

        if(searchSegment.length() > 2) {
            sbCompanyWhereClause
                    .append(" OR company_name LIKE '")
                    .append("%")
                    .append(searchSegment)
                    .append("%'");
        }
        String query = sbSymbolWhereClause.toString() + sbCompanyWhereClause.toString();

        Cursor cursor = Ticker.getCursor(Ticker.class, query, null, "", sortOrder, "50");

        MatrixCursor matrixCursor = new MatrixCursor(
                new String[]{"_ID",
                        SearchManager.SUGGEST_COLUMN_TEXT_1,
                        SearchManager.SUGGEST_COLUMN_TEXT_2,
                        SearchManager.SUGGEST_COLUMN_INTENT_EXTRA_DATA});

        List<Ticker> resultList = new ArrayList<>();
        while(cursor.moveToNext()) {

            String company = cursor.getString(1);
            String symbol = cursor.getString(2);

            resultList.add(new Ticker(symbol, company));
        }

        resultList = sortByBestGuess(resultList, searchSegment);

        for(Ticker ticker : resultList) {
            matrixCursor.addRow(new Object[] {
                    0,                          // Default
                    ticker.getSymbol(),         // Display text 1
                    ticker.getCompanyName(),    // Display text 2
                    ticker.getSymbol()});       // Data key
        }


        return matrixCursor;
    }

    private List<Ticker> sortByBestGuess(List<Ticker> tickers, String search) {
        List<TickerBestGuessScore.TickerScore> scoredTickers = new ArrayList<>();
        for(Ticker ticker : tickers) {
            scoredTickers.add(scorer.score(ticker, search));
        }

        Collections.sort(scoredTickers);

        List<Ticker> sortedList = new ArrayList<>();
        for(TickerBestGuessScore.TickerScore tickerScore : scoredTickers) {

            if(scoredTickers.size() < 15) {
                sortedList.add(tickerScore.getTicker());
                continue;
            }

            if(tickerScore.getScore() > 0) {
                sortedList.add(tickerScore.getTicker());
                continue;
            }

        }
        return sortedList;
    }

    @Nullable
    @Override
    public String getType(@NonNull Uri uri) {
        switch(URI_MATCHER.match(uri)) {
            case TICKER_DATA:
                return TickerData.CONTENT_TYPE;
            case TICKER_DATA_ID:
                return TickerData.CONTENT_TICKER_TYPE;
            default:
                return null;
        }
    }

    @Nullable
    @Override
    public Uri insert(@NonNull Uri uri, @Nullable ContentValues values) {
        return null;
    }

    @Override
    public int delete(@NonNull Uri uri, @Nullable String selection, @Nullable String[] selectionArgs) {
        return 0;
    }

    @Override
    public int update(@NonNull Uri uri, @Nullable ContentValues values, @Nullable String selection, @Nullable String[] selectionArgs) {
        return 0;
    }

}
