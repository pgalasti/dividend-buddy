package com.galasti.dividendbuddy.http;

import com.galasti.dividendbuddy.dto.DividendStatsDto;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestTemplate;

public class HttpFetchStats {

    private String url;

    public HttpFetchStats(String url) {
        this.url = url;
    }

    public DividendStatsDto fetchStats(String symbol) {
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());

        String restUrl = String.format(url, symbol);

        ResponseEntity<DividendStatsDto> response = restTemplate.getForEntity(restUrl, DividendStatsDto.class);
        if(response.getStatusCode() != HttpStatus.OK) {
            throw new HttpServerErrorException(response.getStatusCode());
        }

        return response.getBody();
    }

}
