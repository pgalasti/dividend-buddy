package com.galasti.dividendbuddy.http;

import com.galasti.dividendbuddy.dto.TickerDto;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.List;

public class HttpFetchTicker {

    private String url;

    public HttpFetchTicker(String url) {
        this.url = url;
    }

    public List<TickerDto> fetchTickers() throws HttpServerErrorException {
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());

        ResponseEntity<TickerDto[]> response = restTemplate.getForEntity(url, TickerDto[].class);
        if(response.getStatusCode() != HttpStatus.OK) {
            throw new HttpServerErrorException(response.getStatusCode());
        }

        return Arrays.asList(response.getBody());
    }
}
