package com.galasti.dividendbuddy.http;

import com.galasti.dividendbuddy.dto.ProfileDto;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestTemplate;

public class HttpFetchProfile {

    private String url;

    public HttpFetchProfile(String url) {
        this.url = url;
    }

    public ProfileDto fetchProfile(String symbol) {

        RestTemplate restTemplate = new RestTemplate();
        restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());

        String restUrl = String.format(url, symbol);

        ResponseEntity<ProfileDto> response;
        try {
            response = restTemplate.getForEntity(restUrl, ProfileDto.class);
            if (response.getStatusCode() != HttpStatus.OK) {
                throw new HttpServerErrorException(response.getStatusCode());
            }
        } catch(Exception e) {
            throw new HttpServerErrorException(HttpStatus.NOT_FOUND, "Unable to find symbol resource");
        }

        return response.getBody();


    }
}
