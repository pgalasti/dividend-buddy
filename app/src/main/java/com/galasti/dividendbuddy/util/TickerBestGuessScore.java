package com.galasti.dividendbuddy.util;

import com.galasti.dividendbuddy.persistence.Ticker;

public class TickerBestGuessScore {

    public TickerBestGuessScore() {}


    public TickerScore score(Ticker ticker, String search) {

        TickerScore tickerScore = new TickerScore(ticker);

        String symbol = ticker.getSymbol();
        String company = ticker.getCompanyName();

        if(search.equalsIgnoreCase(symbol)) {
            tickerScore.setScore(100);
            return tickerScore;
        }

        if(search.equalsIgnoreCase(company)) {
            tickerScore.setScore(100);
            return tickerScore;
        }

        if(symbol.toLowerCase().startsWith(search.toLowerCase())) {
            tickerScore.setScore(80);
            return tickerScore;
        }

        if(company.toLowerCase().startsWith(search.toLowerCase())) {
            tickerScore.setScore(80);
            return tickerScore;
        }

        if(search.length() > 3) {
            tickerScore.setScore(10);
            return tickerScore;
        }

        return tickerScore;
    }

    public class TickerScore implements Comparable<TickerScore>{

        protected int score;
        protected Ticker ticker;

        protected TickerScore() {}

        public TickerScore(Ticker ticker) {
            this.ticker = ticker;
            score = 0;
        }

        public Ticker getTicker() {
            return ticker;
        }

        public String getSymbol() {
            return ticker.getSymbol();
        }

        public String getCompanyName() {
            return ticker.getCompanyName();
        }

        public int getScore() {
            return score;
        }

        public void setScore(int score) {
            this.score = score;
        }

        @Override
        public int compareTo(TickerScore otherTicker) {

            return otherTicker.getScore() - this.getScore();
        }
    }
}