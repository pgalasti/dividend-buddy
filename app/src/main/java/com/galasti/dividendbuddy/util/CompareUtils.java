package com.galasti.dividendbuddy.util;

public class CompareUtils {

    public static boolean isNullOrEmpty(String str) {
        return str == null || str.isEmpty();
    }

    public static boolean isNullOrZero(Number number) {
        return number == null || number.equals(0);
    }
}
