package com.galasti.dividendbuddy.activity;

import android.app.Activity;
import android.app.SearchManager;
import android.content.Intent;
import android.os.Bundle;

import com.galasti.dividendbuddy.persistence.LastUpdate;
import com.galasti.dividendbuddy.util.CompareUtils;

import java.util.Date;

public class SearchResultsActivity extends Activity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        handleIntent(getIntent());
    }

    @Override
    protected void onNewIntent(Intent intent) {
        handleIntent(intent);
    }

    private void handleIntent(Intent intent) {

        if (Intent.ACTION_SEARCH.equals(intent.getAction())) {

            String query = this.extractQuery(intent);
            if(!CompareUtils.isNullOrEmpty(query)) {
                Intent profileIntent = new Intent(this.getApplicationContext(), ProfileActivity.class);
                profileIntent.putExtra("symbol", query);
                startActivity(profileIntent);
                this.finish();
            }

        }
    }

    private String extractQuery(Intent intent) {
        String query = intent.getStringExtra(SearchManager.QUERY);
        if(CompareUtils.isNullOrEmpty(query)) {
            query = intent.getStringExtra(SearchManager.EXTRA_DATA_KEY);
        }

        return query;
    }
}
