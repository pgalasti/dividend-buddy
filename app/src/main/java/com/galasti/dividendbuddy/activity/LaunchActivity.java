package com.galasti.dividendbuddy.activity;

import android.app.PendingIntent;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.constraint.ConstraintSet;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.transition.TransitionManager;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.TextView;
import android.widget.Toast;

import com.galasti.dividendbuddy.R;
import com.galasti.dividendbuddy.persistence.LastUpdate;
import com.galasti.dividendbuddy.services.FetchTickerReceiver;

import java.util.Date;

public class LaunchActivity extends AppCompatActivity {

    private android.widget.SearchView tickerSearchView;
    private ConstraintLayout constraintLayout;

    private PendingIntent pendingIntent;

    @Override
    protected void onStart() {
        super.onStart();

        if(this.isOutOfDate()) {
            Intent alarmIntent = new Intent(this, FetchTickerReceiver.class);
            this.pendingIntent = PendingIntent.getBroadcast(this, 0, alarmIntent, 0);

            sendBroadcast(alarmIntent);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_launch);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        this.tickerSearchView = findViewById(R.id.launch_ticker_search_view);
        this.constraintLayout = findViewById(R.id.main_constraint);

        this.tickerSearchView.setOnQueryTextFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                ConstraintSet constraintSet = new ConstraintSet();
                constraintSet.clone(getBaseContext(), hasFocus ? R.layout.content_launch_search_mode : R.layout.content_launch);
                TransitionManager.beginDelayedTransition(constraintLayout);
                constraintSet.applyTo(constraintLayout);
            }
        });

        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        tickerSearchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        tickerSearchView.setMaxWidth(Integer.MAX_VALUE);
        tickerSearchView.setOnQueryTextListener(new android.widget.SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                // This disables manual submits
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });

    }

    private final static long MILLIS_PER_DAY = 24*60*60*1000;
    private boolean isOutOfDate() {
        LastUpdate lastUpdated = LastUpdate.findById(LastUpdate.class, 1);
        if(lastUpdated == null) {
            return true;
        }

        Date lastUpdate = lastUpdated.getLastUpdateTimestamp();

        Date now = new Date();
        return Math.abs(now.getTime() - lastUpdate.getTime()) > MILLIS_PER_DAY*7;
    }

}
