package com.galasti.dividendbuddy.activity;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.constraint.ConstraintSet;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.transition.AutoTransition;
import android.transition.TransitionManager;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.galasti.dividendbuddy.R;
import com.galasti.dividendbuddy.dto.DividendStatsDto;
import com.galasti.dividendbuddy.dto.ProfileDto;
import com.galasti.dividendbuddy.http.HttpFetchProfile;
import com.galasti.dividendbuddy.http.HttpFetchStats;
import com.galasti.dividendbuddy.util.CompareUtils;

import org.springframework.web.client.HttpServerErrorException;

import java.math.RoundingMode;

public class ProfileActivity extends AppCompatActivity {

    private ConstraintLayout constraintLayout;
    private TextView symbolTextView;
    private TextView companyNameTextView;
    private TextView dividendRateView;
    private TextView dividendYieldView;
    private ProgressBar headerProgressBar;
    private ProgressBar dividendProgressBar;
    private String symbol;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ticker_activity_loaded);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        this.symbol = getIntent().getStringExtra("symbol");

        this.constraintLayout = findViewById(R.id.profile_parent);
        this.symbolTextView = findViewById(R.id.profile_ticker_symbol);
        this.dividendRateView = findViewById(R.id.dividend_rate);
        this.dividendYieldView = findViewById(R.id.dividend_yield);
        this.companyNameTextView = findViewById(R.id.profile_ticker_description);
        this.headerProgressBar = findViewById(R.id.profile_header_progress_bar);
        this.dividendProgressBar = findViewById(R.id.dividend_progress_bar);
        this.symbolTextView.setText(this.symbol.toUpperCase());

        this.headerProgressBar.setVisibility(View.VISIBLE);
        this.symbolTextView.setVisibility(View.GONE);
        this.companyNameTextView.setVisibility(View.GONE);

        this.dividendProgressBar.setVisibility(View.VISIBLE);
        this.dividendRateView.setVisibility(View.GONE);
        this.dividendYieldView.setVisibility(View.GONE);

        new FetchProfileTask().execute(symbol);
        new FetchStatsTask().execute(symbol);
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        ConstraintSet constraintSet = new ConstraintSet();
        constraintSet.clone(getBaseContext(), R.layout.ticker_activity_loaded);

        AutoTransition autoTransition = new AutoTransition();
        autoTransition.setDuration(750);

        TransitionManager.beginDelayedTransition(constraintLayout, autoTransition);
//        constraintSet.applyTo(constraintLayout);
    }

    private class FetchProfileTask extends AsyncTask<String, Void, ProfileDto> {

        private HttpFetchProfile httpFetchProfile;

        @Override
        protected ProfileDto doInBackground(String... symbols) {
            String restUrl = getString(R.string.http_fetch_profile);
            this.httpFetchProfile = new HttpFetchProfile(restUrl);

            String symbol = symbols[0];

            ProfileDto profile;
            try {
                profile = this.httpFetchProfile.fetchProfile(symbol);
            } catch (HttpServerErrorException e) {
                Log.d("FetchProfileTask", "Unable to find symbol!");
                profile = new ProfileDto();
            }
            return profile;
        }

        @Override
        protected void onPostExecute(ProfileDto profileDto) {
            super.onPostExecute(profileDto);
            String companyName = profileDto.getCompanyName();
            if(CompareUtils.isNullOrEmpty(companyName)) {
                Toast.makeText(getBaseContext(), "There was an error searching for a ticker.", Toast.LENGTH_SHORT).show();
            }
            companyNameTextView.setText(companyName);

            symbolTextView.setVisibility(View.VISIBLE);
            companyNameTextView.setVisibility(View.VISIBLE);
            headerProgressBar.setVisibility(View.GONE);
        }
    }

    private class FetchStatsTask extends AsyncTask<String, Void, DividendStatsDto> {

        private HttpFetchStats httpFetchStats;

        @Override
        protected DividendStatsDto doInBackground(String... symbols) {
            String restUrl = getString(R.string.http_fetch_stats);
            this.httpFetchStats = new HttpFetchStats(restUrl);

            String symbol = symbols[0];

            DividendStatsDto stats;
            try {
                stats = this.httpFetchStats.fetchStats(symbol);
            } catch (HttpServerErrorException e) {
                Log.d("FetchProfileTask", "Unable to find symbol for dividend stats!");
                stats = new DividendStatsDto();
            }
            return stats;
        }

        @Override
        protected void onPostExecute(DividendStatsDto statsDto) {
            super.onPostExecute(statsDto);

            dividendProgressBar.setVisibility(View.GONE);
            dividendRateView.setVisibility(View.VISIBLE);
            dividendYieldView.setVisibility(View.VISIBLE);
            dividendRateView.setText("$" + statsDto.getDividendRate().toString());
            dividendYieldView.setText(statsDto.getDividendYield().setScale(3, RoundingMode.HALF_EVEN) + "%");
//            String companyName = profileDto.getCompanyName();
//            if(CompareUtils.isNullOrEmpty(companyName)) {
//                Toast.makeText(getBaseContext(), "There was an error searching for a ticker.", Toast.LENGTH_SHORT).show();
//            }
//            companyNameTextView.setText(companyName);
//
//            symbolTextView.setVisibility(View.VISIBLE);
//            companyNameTextView.setVisibility(View.VISIBLE);
//            headerProgressBar.setVisibility(View.GONE);
        }
    }

//
}
